# doom.d

# Tips && Trick
## When you want to use jupyter-python, the following is required in the header for it to work
"#+PROPERTY: header-args:jupyter-python :session session_name kernel: python3 :async yes"
I don't know why, but that's just the way it is.

Now, a src block can be made as 
#+begin_src jupyter-python :session session_name :kernel python3
print("Hello World!")
#+end_src


